int aktiveAutoStep = 0;
double timeStepStart;

//Lichtschranken 
int LSp1 = 51;
int LSp2 = 44;
int LSp3 = 40;
int LSp4 = 44;
int LSp5 = 42;
int LSb = 50;
int lastStateLSb = LOW;

//FallrohrPostition
double FallRohrAktuel = 0;
double FallRohrPos1 = 1500;
double FallRohrPos2 = 100;
double FallRohrPos3 = 500;
int dir = 53;
int pull = 52;

//Förderband
int FBv = 4;
int FPr = 6;
int FPv = 8;

//Zylinder 
int Zylinder_unten = 80;
int Zylinder_oben = 5;

//USER_INPUT
int Stopp = 30;
int Start = 34;
int PlatteEntnommen = 32;

//Lampen
int P_Aktive = 18;
int P_Entnehmen = 20;

void setup() {
	pinMode(LSp1, INPUT);
	pinMode(LSp2, INPUT);
	pinMode(LSp3, INPUT);
	pinMode(LSp4, INPUT);
	pinMode(LSp5, INPUT);
	pinMode(LSb, INPUT);
	pinMode(dir, OUTPUT);
	pinMode(pull, OUTPUT);
  pinMode(FPv, OUTPUT);
  pinMode(FPr, OUTPUT);
	pinMode(FBv, OUTPUT);
  digitalWrite(FBv, 1);
  digitalWrite(FPv, 1);
  digitalWrite(FPr, 1);
	pinMode(Zylinder_unten, OUTPUT);
	pinMode(Zylinder_oben, OUTPUT);
  digitalWrite(Zylinder_oben, 1);
  digitalWrite(Zylinder_unten, 1);
	pinMode(Stopp, INPUT);
	pinMode(Start, INPUT);
	pinMode(PlatteEntnommen, INPUT);
	pinMode(P_Aktive, OUTPUT);
	pinMode(P_Entnehmen, OUTPUT);
	Serial.begin(9600);
	startStep(aktiveAutoStep);
}

void buttonStop() {
	startStep(0);
}

bool dr(int pin) { return digitalRead(pin); }
bool getLSBecher() { return dr(LSb); }
bool getLSp1() { return dr(LSp1); }
bool getLSp2() { return dr(LSp2); }
bool getLSp3() { return dr(LSp3); }
bool getLSp4() { return dr(LSp4); }
bool getLSp5() { return dr(LSp5); }

void fahreBisBecher(){
	getLSBecher()?
		digitalWrite(FBv, 1):
		digitalWrite(FBv, 0);
}

void fahreBisNeuerBecher(){
	getLSBecher() && lastStateLSb == LOW ?
		digitalWrite(FBv, 1):
		digitalWrite(FBv, 0);
}

void modeAuto() {
	switch (aktiveAutoStep) {
	case 0:
		digitalWrite(P_Aktive, 0);
		digitalWrite(FBv, 1);
		digitalWrite(FPv, 1);
		digitalWrite(FPr, 1);
		if (dr(Stopp) && dr(Start) && getLSp1()) {
      startStep(1);
		}
		break;
	case 1:
		digitalWrite(P_Aktive, 1);
		fahreBisBecher();
		bewegeFallrohr(FallRohrPos1);
		if (getLSp2()) {
      digitalWrite(FPv, 1);
    }
    else {
      digitalWrite(FPv, 0);
    }
		digitalWrite(Zylinder_unten, 1);
		digitalWrite(Zylinder_oben, 1);

		if (getLSBecher() && FallRohrAktuel == FallRohrPos1 && getLSp2()) {
			digitalWrite(FBv, 1);
			digitalWrite(FPv, 1);
			startStep(2);
		}
		break;
	case 2:
		digitalWrite(Zylinder_unten, 1);

		if ((millis() - timeStepStart) >= 500) {
			startStep(3);
		}
		break;
	case 3:
		if (lastStateLSb == LOW && getLSBecher()) {
			digitalWrite(FBv, 1);
			startStep(4);
		}
		else {
			digitalWrite(FBv, 0);
		}
		break;
	case 4:
		digitalWrite(Zylinder_oben, 1);

		if ((millis() - timeStepStart) >= 500) {
			startStep(5);
		}
		break;
	case 5:
		if (lastStateLSb == LOW && getLSBecher()) {
			digitalWrite(FBv, 1);
			startStep(6);
		}
		else {
			digitalWrite(FBv, 0);
		}
		break;
	case 6:
		bewegeFallrohr(FallRohrPos2);

		if (FallRohrAktuel == FallRohrPos2) {
			startStep(7);
		}
		break;
	case 7:
		digitalWrite(Zylinder_unten, 0);

		if ((millis() - timeStepStart) >= 2000) {
			startStep(8);
		}
		break;
	case 8:
		bewegeFallrohr(FallRohrPos3);

		if (FallRohrAktuel == FallRohrPos3) {
			startStep(9);
		}
		break;
	case 9:
		digitalWrite(Zylinder_oben, 0);

		if ((millis() - timeStepStart) >= 2000) {
			startStep(10);
		}
		break;
	case 10:
		bewegeFallrohr(FallRohrPos1);
		if (getLSp3()) {
			digitalWrite(FPv, 1);
		}
		else {
			digitalWrite(FPv, 0);
		}
		fahreBisBecher();

		if (FallRohrAktuel == FallRohrPos1 && getLSp3() && getLSBecher()) {
			digitalWrite(FPv, 1);
			digitalWrite(FBv, 1);
			startStep(11);
		}
		break;
	case 11:
		digitalWrite(Zylinder_unten, 1);

		if ((millis() - timeStepStart) >= 500) {
			startStep(12);
		}
		break;
	case 12:
		if (lastStateLSb == LOW && getLSBecher()) {
			digitalWrite(FBv, 1);
			startStep(13);
		}
		else {
			digitalWrite(FBv, 0);
		}
		break;
	case 13:
		digitalWrite(Zylinder_oben, 1);

		if ((millis() - timeStepStart) >= 500) {
			startStep(14);
		}
		break;
	case 14:
		if (lastStateLSb == LOW && getLSBecher()) {
			digitalWrite(FBv, 1);
			startStep(15);
		}
		else {
			digitalWrite(FBv, 0);
		}
		break;
	case 15:
		bewegeFallrohr(FallRohrPos2);

		if (FallRohrAktuel == FallRohrPos2) {
			startStep(16);
		}
		break;
	case 16:
		digitalWrite(Zylinder_unten, 0);

		if ((millis() - timeStepStart) >= 2000) {
			startStep(17);
		}
		break;
	case 17:
		bewegeFallrohr(FallRohrPos3);

		if (FallRohrAktuel == FallRohrPos3) {
			startStep(18);
		}
		break;
	case 18:
		digitalWrite(Zylinder_oben, 0);

		if ((millis() - timeStepStart) >= 2000) {
			startStep(19);
		}
		break;
	case 19:
		bewegeFallrohr(FallRohrPos1);
		if (getLSp4()) {
			digitalWrite(FPv, 1);
		}
		else {
			digitalWrite(FPv, 0);
		}
		fahreBisBecher();

		if (FallRohrAktuel == FallRohrPos1 && getLSp4() && getLSBecher()) {
			digitalWrite(FPv, 1);
			digitalWrite(FBv, 1);
			startStep(20);
		}
		break;
	case 20:
		digitalWrite(Zylinder_unten, 1);

		if ((millis() - timeStepStart) >= 500) {
			startStep(21);
		}
		break;
	case 21:
		if (lastStateLSb == LOW && getLSBecher()) {
			digitalWrite(FBv, 1);
			startStep(22);
		}
		else {
			digitalWrite(FBv, 0);
		}
		break;
	case 22:
		digitalWrite(Zylinder_oben, 1);

		if ((millis() - timeStepStart) >= 500) {
			startStep(23);
		}
		break;
	case 23:
		if (lastStateLSb == LOW && getLSBecher()) {
			digitalWrite(FBv, 1);
			startStep(24);
		}
		else {
			digitalWrite(FBv, 0);
		}
		break;
	case 24:
		bewegeFallrohr(FallRohrPos2);

		if (FallRohrAktuel == FallRohrPos2) {
			startStep(25);
		}
		break;
	case 25:
		digitalWrite(Zylinder_unten, 0);

		if ((millis() - timeStepStart) >= 2000) {
			startStep(26);
		}
		break;
	case 26:
		bewegeFallrohr(FallRohrPos3);

		if (FallRohrAktuel == FallRohrPos3) {
			startStep(27);
		}
		break;
	case 27:
		digitalWrite(Zylinder_oben, 0);

		if ((millis() - timeStepStart) >= 2000) {
			startStep(28);
		}
		break;
	case 28:
		bewegeFallrohr(FallRohrPos1);
		if (getLSp5()) {
			digitalWrite(FPv, 0);
		}
		else {
			digitalWrite(FPv, 1);
		}
		fahreBisBecher();

		if (FallRohrAktuel == FallRohrPos1 && getLSp5() && getLSBecher()) {
			digitalWrite(FPv, 1);
			digitalWrite(FBv, 1);
			startStep(29);
		}
		break;
	case 29:
		digitalWrite(P_Entnehmen, 1);

		if (dr(PlatteEntnommen)) {
			digitalWrite(P_Entnehmen, 0);
			startStep(30);
		}
		break;
	case 30:
		digitalWrite(FPr, 1);
		if (getLSp1()) {
			digitalWrite(FPr, 0);
			startStep(0);
		}
		break;
	}
	lastStateLSb = getLSBecher();
}
void startStep(int Step) {
	setTimeStepStart();
	aktiveAutoStep = Step;
	Serial.println((String) "AktiveStep: " + Step + " | StartTime: " + timeStepStart + " | FallrohrPostition: " + FallRohrAktuel);
}
void setTimeStepStart() {
	timeStepStart = millis();
}
void bewegeFallrohr(double pos) {
	if (FallRohrAktuel < pos) {
		digitalWrite(dir, 0);
		digitalWrite(pull, LOW);
		delay(1);
		digitalWrite(pull, HIGH);
		FallRohrAktuel++;
	}
	if (FallRohrAktuel > pos) {
		digitalWrite(dir, 1);
		digitalWrite(pull, LOW);
		delay(1);
		digitalWrite(pull, HIGH);
		FallRohrAktuel--;
	}
	digitalWrite(dir, 0);
}

void loop() {
	if (!digitalRead(Stopp)) { buttonStop(); }
	modeAuto();
}